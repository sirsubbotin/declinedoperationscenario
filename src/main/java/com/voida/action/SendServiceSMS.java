package com.voida.action;

import com.voida.eyeflex.sdk.action.AbstractAction;
import com.voida.eyeflex.sdk.action.ActionContext;
import com.voida.eyeflex.sdk.action.setup.ActionClassRegister;
import com.voida.eyeflex.sdk.exceptions.ActionNotFoundException;
import com.voida.eyeflex.sdk.model.ActionParam;
import com.voida.scenario.DeclineOperation;
import com.voida.util.DynamicEnrichment;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class SendServiceSMS extends AbstractAction {

    private static Logger logger = Logger.getLogger(DeclineOperation.class);

    private String actionName;
    private String dontSendAfter;
    private String dontSendBefore;
    private String keepSms;

    private static final String ACTION_TEMPLATE_NAME = "SMS";

    private static final String MSISDN = "MSISDN";
    private static final String MESSAGE = "MESSAGE";
    private static final String DONT_SEND_SMS_AFTER = "DONT SEND SMS AFTER";
    private static final String DONT_SEND_SMS_BEFORE = "DONT SEND SMS BEFORE";
    private static final String KEEP_SMS_UNTIL_VALID_TIME = "KEEP SMS";
    private static final String ACTION_CHANNEL = "ACTION CHANNEL";
    private static final String RETRY_CHANNEL = "RETRY CHANNEL";


    public SendServiceSMS(String actionName, String dontSendAfter, String dontSendBefore, String keepSms) {
        this.actionName = actionName;
        this.dontSendAfter = dontSendAfter;
        this.dontSendBefore = dontSendBefore;
        this.keepSms = keepSms;
    }

    public int execute(ActionContext actionContext) throws Exception {

        logger.debug("SendServiceSMS Action: " + actionName);
        logger.debug("SendServiceSMS Action Context: " + actionContext);

        AbstractAction smsHandler;

        try {
            smsHandler = ActionClassRegister.getInstance().getAction("SMS");
        } catch (ActionNotFoundException e) {
            logger.error(e.getMessage(), e);
            return 0;
        }

        String RBS = String.valueOf(actionContext.getParameter("RBS"));
        String allCard = String.valueOf(actionContext.getParameter("UOWCARDS"));
        String groupUow = String.valueOf(actionContext.getParameter("UOWDTL")).trim();

        String card = allCard.split(";")[0];

        String group = getGroup(groupUow);

        logger.debug("SendServiceSMS details: RBS - " + RBS + ", card - " + card + ", groupUow - " + groupUow + ", group - " + group);

        String fullname = DynamicEnrichment.getInstance().getFullName(RBS);
        String phoneNumber = DynamicEnrichment.getInstance().getPhoneNumber(RBS);

        if (phoneNumber == null) {
            logger.debug("VALIDATION - PHONE NUMBER FOR RBS=[" + RBS + "] IS NULL.");
            return 0;
        }

        if (group == null) {
            return 0;
        }

        ActionContext smsContext = new ActionContext();

        smsContext.setScenarioKey(RBS);
        smsContext.setOutputActionTemplateName(ACTION_TEMPLATE_NAME);
        smsContext.setOutputActionName(actionName);
        smsContext.setScenarioName(DeclineOperation.class.getSimpleName());
        smsContext.setMessage("");

        smsContext.setParameter(MSISDN, phoneNumber);
        smsContext.setParameter(MESSAGE, DeclineOperation.SMS_CONTENT.get(actionName).replace("|name|", fullname).replace("|card|", card).replace("|group|", group));
        smsContext.setParameter(DONT_SEND_SMS_AFTER, dontSendAfter);
        smsContext.setParameter(DONT_SEND_SMS_BEFORE, dontSendBefore);
        smsContext.setParameter(KEEP_SMS_UNTIL_VALID_TIME, keepSms);
        smsContext.setParameter(ACTION_CHANNEL, "VIBER");
        smsContext.setParameter(RETRY_CHANNEL, "SMS");

        logger.debug("SendServiceSMS Context: " + smsContext);

        try {
            smsHandler.execute(smsContext);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.warn("MISSED SMS: " + smsContext.toString());
        }

        return 1;
    }

    public List<ActionParam> getParams() {
        return new ArrayList<ActionParam>() {{
            add(new ActionParam("RBS", null, null));
            add(new ActionParam("UOWCARDS", null, null));
            add(new ActionParam("UOWDTL", null, null));
        }};
    }

    private String getGroup(String trxDetail) {
        return DeclineOperation.GROUP.get(trxDetail);
    }
}