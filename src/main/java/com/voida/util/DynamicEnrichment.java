package com.voida.util;

import com.voida.eyeflex.sdk.database.ExternalSource;
import com.voida.eyeflex.sdk.database.ExternalSourceProvider;
import com.voida.scenario.DeclineOperation;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DynamicEnrichment {

    private static DynamicEnrichment instance;
    private static Logger logger = Logger.getLogger(DeclineOperation.class);

    public static synchronized DynamicEnrichment getInstance() {
        if (instance == null) {
            instance = new DynamicEnrichment();
        }
        return instance;
    }

    public String getPhoneNumber(String RBS) {
        String SQL = "select TRIM(c.clientmobilephone) as phone from prod_table p, cust_table c " +
                "where  p.rbs = ? and  p.id = c.id and c.clientmobilephone is not null  and rownum = 1";
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String result = null;

        try {
            ExternalSource source = ExternalSourceProvider.createConnection("ALFABANK");

            cn = source.getConnection();
            ps = cn.prepareStatement(SQL);
            ps.setString(1, RBS);

            rs = ps.executeQuery();

            if (rs.next()) {
                result = rs.getString("phone");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (cn != null) cn.close();
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return result;
    }

    public String getFullName(String RBS) {
        String SQL = "select INITCAP(SUBSTR(TRIM(c.clientfullname),INSTR(c.clientfullname, ' '))) as fullname from prod_table p , cust_table c " +
                "where  p.rbs = ? and  p.id = c.id and rownum = 1";
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String result = null;

        try {
            ExternalSource source = ExternalSourceProvider.createConnection("ALFABANK");

            cn = source.getConnection();
            ps = cn.prepareStatement(SQL);
            ps.setString(1, RBS);

            rs = ps.executeQuery();

            if (rs.next()) {
                result = rs.getString("fullname");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (cn != null) cn.close();
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return result;
    }

    private long executeQuery(String SQL, String PARAM) {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        long result = -1;

        try {
            ExternalSource source = ExternalSourceProvider.createConnection("ALFABANK");

            cn = source.getConnection();
            ps = cn.prepareStatement(SQL);
            ps.setString(1, PARAM);

            rs = ps.executeQuery();

            if (rs.next()) {
                result = rs.getLong("result");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (cn != null) cn.close();
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return result;
    }

    private double executeQueryDouble(String SQL, String PARAM) {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        double result = -1;

        try {
            ExternalSource source = ExternalSourceProvider.createConnection("ALFABANK");

            cn = source.getConnection();
            ps = cn.prepareStatement(SQL);
            ps.setString(1, PARAM);

            rs = ps.executeQuery();

            if (rs.next()) {
                result = rs.getDouble("result") == 0 ? -1 : rs.getDouble("result");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (cn != null) cn.close();
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return result;
    }
}
