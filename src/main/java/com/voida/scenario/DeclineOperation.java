package com.voida.scenario;


import com.voida.action.SendServiceSMS;
import com.voida.eyeflex.sdk.database.dao.Scenario;
import com.voida.eyeflex.sdk.database.dao.ScenarioStatus;
import com.voida.eyeflex.sdk.scenario.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class DeclineOperation extends AbstractScenario {

    private static final HashMap<String, Object> internalEventParams = new HashMap<String, Object>() {{
        put("RBS", String.class);
        put("UOWCARDS", String.class);
        put("UOWDTL", String.class);
    }};

    public static final HashMap<String, String> SMS_CONTENT = new HashMap<String, String>() {{

        put("SERVICE_SMS", "Здравствуйте, |name|! Мы обратили внимание, что по Вашей карте |card| происходят неуспешные попытки списания средств в пользу |group|. Если Вы пользуетесь этой услугой, просим пополнить счет для осуществления оплаты. Если эта услуга Вам больше не нужна, погасите задолженность и проверьте актуальность способа оплаты по вашим подпискам и сервисам. Как это сделать: http://bit.ly/2m6eWSx. Ваш Альфа-Банк!");

    }};

    public static final HashMap<String, String> GROUP = new HashMap<String, String>() {{

        put("ITUNES.COM/BILL", "apple");
        put("Yandex.Taxi", "yandex");
        put("UBER", "uber");
        put("MICROSOFT   *", "microsoft");
        put("GOOGLE", "google");
        put("UNITY LAUNDRY SYSTEMS", "unity");
        put("AMZN Digital", "amazon");
        put("AliExpress.com", "aliexpress");
        put("RINGOSTAT", "ringostat");
        put("NETFLIX.COM", "netflix");
        put("AIRBNB", "airbnb");
        put("SPYFU.COM", "spyfu");
        put("MEGOGO TV AND MOVIES", "megogo");
        put("Dropbox", "dropbox");
        put("ODNOKLASSNIKI.RU", "odnoklassniki");
        put("STEAMGAMES.COM", "steam");
        put("FACEBK", "facebk");
        put("VK.COM", "vk");


    }};

    public DeclineOperation() throws Exception {
        super();
    }


    @Override
    public void initialize() throws Exception {

        String STATE = getClass().getSimpleName().toUpperCase();

        EventType log = EventTypeFactory.build("LOG", new HashMap<String, Object>() {{
            put("LOG", String.class);
        }});
        registerEventType(log);

        Statement stateTable = StatementFactory.build("STATE_TABLE", "CREATE TABLE " + STATE
                + " (RBS STRING PRIMARY KEY, CURR_STATE STRING, PREV_STATE STRING, TRX_COUNT INT)");
        registerStatement(stateTable);

        EventType sendSmsServiceEvent = EventTypeFactory.build("SEND_SMS_SERVICE", internalEventParams);
        registerEventType(sendSmsServiceEvent);

        Statement checkTransactionStatement = StatementFactory.build("SERVICE_ACTION", "ON ALL_UOWDATAPF(UOWTR NOT IN ('SOA SPISANIE', 'SOA ZACHISLENI'), UOWTRT = 'AUTH_T_Q_051' ) X MERGE " + STATE + " T " +
                " WHERE T.RBS = X.UOWACC " +
                " WHEN NOT MATCHED " +
                "   THEN INSERT SELECT 'tracking' AS CURR_STATE, 'notalive' AS PREV_STATE, X.UOWACC AS RBS, 0 AS TRX_COUNT " +
                " WHEN MATCHED AND TRX_COUNT < 5 " +
                "   THEN UPDATE SET PREV_STATE = CURR_STATE, CURR_STATE = 'notyet', TRX_COUNT = TRX_COUNT + 1 " +
                " WHEN MATCHED AND TRX_COUNT >= 5 AND CURR_STATE != 'sms'" +
                "   THEN INSERT INTO " + sendSmsServiceEvent.getName() + " SELECT T.RBS AS RBS, X.UOWCARDS AS UOWCARDS, X.UOWDTL AS UOWDTL" +
                "   THEN UPDATE SET PREV_STATE = CURR_STATE, CURR_STATE = 'sms' ");
        registerStatement(checkTransactionStatement);

        Statement sendSMSService = StatementFactory.build("SEND_SMS_NO_MONEY", "SELECT * FROM " + sendSmsServiceEvent.getName());
        sendSMSService.addAction(new SendServiceSMS("SERVICE_SMS", "22", "9", "1"));
        registerStatement(sendSMSService);

    }

    @Override
    public Scenario getScenario() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 10);

        return new Scenario(
                this.getClass().getName(),
                new Date(),
                calendar.getTime(),
                ScenarioStatus.RUNNING,
                "Declined trx",
                "declined service trx"
        );
    }
}
